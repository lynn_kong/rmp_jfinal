package com.dcits.dto;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.dcits.business.userconfig.UserSpace;

/**
 * 将数据内容转化为前端需要的格式
 * @author xuwangcheng
 * @version 2018.3.20
 *
 */
public class DTOParseUtil {
	
	public static JSONArray parseUserSpace() {
		JSONArray spaces = new JSONArray();
		for (UserSpace space:UserSpace.getSpaces().values()) {
			JSONObject o = new JSONObject();
			o.put("userKey", space.getUserConfig().getUserKey());
			o.put("createTime", space.getUserConfig().getCreateTime());
			o.put("serverCount", space.getServerCount());
			spaces.add(o);
		}		
		return spaces;
	}
}
