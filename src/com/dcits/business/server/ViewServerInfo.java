package com.dcits.business.server;

import com.dcits.mvc.common.model.ServerInfo;

/**
 * 视图信息通用类，用于给前端数据展示用
 * @author xuwangcheng
 * @version 1.0.0.0, 2018.3.28
 *
 */
public abstract class ViewServerInfo extends ServerInfo {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static Integer ID_COUNT = 0; 
	
	protected Integer viewId;
	
	protected MonitoringInfo info;
	
	protected String connectStatus;
	
	/**
	 * 记录命令执行或者信息获取失败的信息
	 */
	protected String commandExecErrorMsg;

	public ViewServerInfo() {
		super();
		this.viewId = ID_COUNT++;
		// TODO Auto-generated constructor stub
	}
	
	public ViewServerInfo(MonitoringInfo info) {
		this();
		this.info = info;
	}
	
	/********************************************************/
	/**
	 * 连接监控服务器
	 * @return flag = true 为正常 其他为异常信息
	 */
	public abstract String connect();
	/**
	 * 断开连接
	 * @return
	 */
	public abstract boolean disconect();
	/**
	 * 获取监控信息
	 */
	public abstract void getMonitoringInfo();	
	/********************************************************/
	public void setBaseServerInfo(ServerInfo serverInfo) {
		// TODO Auto-generated method stub
		this.setHost(serverInfo.getHost());
		this.setRealHost(serverInfo.getRealHost());
		this.setPort(serverInfo.getPort());
		this.setUsername(serverInfo.getUsername());
		this.setServerType(serverInfo.getServerType());
		this.setTags(serverInfo.getTags());
		this.setPassword(serverInfo.getPassword());
		this.setParameters(serverInfo.getParameters());
		this.setId(serverInfo.getId());
	}
	
	public void setConnectStatus(String connectStatus) {
		this.connectStatus = connectStatus;
	}
	
	public void setCommandExecErrorMsg(String commandExecErrorMsg) {
		this.commandExecErrorMsg = commandExecErrorMsg;
	}
	
	public String getCommandExecErrorMsg() {
		return commandExecErrorMsg;
	}
	
	public String getConnectStatus() {
		return connectStatus;
	}
	
	public Integer getViewId() {
		return viewId;
	}

	public void setViewId(Integer viewId) {
		this.viewId = viewId;
	}

	public MonitoringInfo getInfo() {
		return info;
	}

	public void setInfo(MonitoringInfo info) {
		this.info = info;
	}

}
