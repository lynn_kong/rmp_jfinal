package com.dcits.business.server.linux;

import java.util.HashMap;
import java.util.Map;

import com.dcits.business.server.MonitoringInfo;

public class LinuxMonitoringInfo extends MonitoringInfo{
	
	public static final String TCP_ESTABLISHED = "ESTABLISHED";
	public static final String TCP_CLOSE_WAIT = "CLOSE_WAIT";
	public static final String TCP_LISTEN = "LISTEN";
	public static final String TCP_TIME_WAIT = "TIME_WAIT";
	
	public static final String NETWORK_RX = "rx";
	public static final String NETWORK_TX = "tx";
	
	public static final String DISK_ROOT = "rootDisk";
	public static final String DISK_USER = "userDisk";
	
	public static final String CPU_FREE = "freeCpu";
	public static final String MEMORY_FREE = "freeMem";
	
	public static final String IO_WAIT = "ioWait";
	
	public static final String IO_READ = "ioRead";
	public static final String IO_WRITE = "ioWrite";
	
	private String freeCpu;
	private String ioWait;
	private String freeMem;
	
	private Map<String, Object> diskInfo = new HashMap<String, Object>();
	private Map<String, Object> tcpInfo = new HashMap<String, Object>();
	private Map<String, Object> networkInfo = new HashMap<String, Object>();
	private Map<String, Object> ioInfo = new HashMap<String, Object>();
	
	
	public void setIoInfo(Map<String, Object> ioInfo) {
		this.ioInfo = ioInfo;
	}
	
	public Map<String, Object> getIoInfo() {
		return ioInfo;
	}

	public String getFreeCpu() {
		return freeCpu;
	}

	public void setFreeCpu(String freeCpu) {
		this.freeCpu = freeCpu;
	}

	public String getIoWait() {
		return ioWait;
	}

	public void setIoWait(String ioWait) {
		this.ioWait = ioWait;
	}

	public String getFreeMem() {
		return freeMem;
	}

	public void setFreeMem(String freeMem) {
		this.freeMem = freeMem;
	}

	public Map<String, Object> getDiskInfo() {
		return diskInfo;
	}

	public void setDiskInfo(Map<String, Object> diskInfo) {
		this.diskInfo = diskInfo;
	}

	public Map<String, Object> getTcpInfo() {
		return tcpInfo;
	}

	public void setTcpInfo(Map<String, Object> tcpInfo) {
		this.tcpInfo = tcpInfo;
	}

	public Map<String, Object> getNetworkInfo() {
		return networkInfo;
	}

	public void setNetworkInfo(Map<String, Object> networkInfo) {
		this.networkInfo = networkInfo;
	}
	
}
