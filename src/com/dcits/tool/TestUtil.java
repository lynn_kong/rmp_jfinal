package com.dcits.tool;

import java.io.File;
import java.io.IOException;

import org.junit.Test;

import com.alibaba.fastjson.JSONObject;
import com.dcits.business.server.redis.RedisServer;
import com.jfinal.kit.PathKit;

public class TestUtil {
	@Test
	public void test1() throws Exception{
		String command = PathKit.getWebRootPath() + File.separator + "tools" + File.separator + "redis" 
				+ File.separator + "redis-cli_win64.exe" + " -h 122.112.246.117 -p 6379 info";
		System.out.println(RmpUtil.execCommand(command));
	}
	
	@Test
	public void test2() throws IOException {
		//122.112.246.117 -p 6379
		RedisServer server = new RedisServer();
		server.setHost("122.112.246.117");
		server.setPort(6379);
		server.getMonitoringInfo();
		server.connect();
		System.out.println(server.request("set ui www"));;
	}
	
	@Test
	public void test3() {
		String json = "{\"name\":\"aaa\", \"age\":20}";
		JSONObject a = JSONObject.parseObject(json);
		System.out.println(a.getString("bbb"));
	}
}	
